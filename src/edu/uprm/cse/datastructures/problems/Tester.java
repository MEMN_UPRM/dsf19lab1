package edu.uprm.cse.datastructures.problems;

import static org.junit.Assert.*;

import org.junit.Test;

import edu.uprm.cse.datastructures.bag.Bag;
import edu.uprm.cse.datastructures.bag.DynamicBag;
import edu.uprm.cse.datastructures.set.ArraySet;
import edu.uprm.cse.datastructures.set.Set;

public class Tester {

	@Test
	public void mostFrequentTest1() {
		Bag<Integer> B1 = new DynamicBag<Integer>();
		
		B1.add(1);
		B1.add(1);
		B1.add(2);
		B1.add(2);
		B1.add(2);
		B1.add(4);
		B1.add(3);
		B1.add(3);
		B1.add(3);
		B1.add(2);

		Bag<Integer> B2 = B1.mostFrequentThan(1);
		
		Object[] B3 = B2.toArray();
		
		for(Object i: B3) {
			if(((Integer) i) != 2 && ((Integer) i) != 3)
				fail("Wrong value was given. Expected answer is 2 and 3.");
		}
		assertTrue(B3.length == 2);
		}
	
	@Test
	public void mostFrequentTest2() {
		Bag<Integer> B1 = new DynamicBag<Integer>();
		
		B1.add(1);
		B1.add(1);
		B1.add(2);
		B1.add(2);
		B1.add(2);
		B1.add(4);
		
	}
}

	